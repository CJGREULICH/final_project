## Final Project

This repository contains my final project. Within it, you will find my notebook,
my environment.yml file, a .csv file that is from the Storm Prediction Center,
and multiple netCDF files containing ERA5 re-analysis data and gridded data from the SPC.

The 1950-2022 Actual Tornadoes csv file contains all the information from the SPC about tornadoes
that occurred within the time frame of the filename. We will be extracting this and looking 
at the data throughout the first part of the notebook.

The alltorn.nc is the gridded netCDF file from the SPC which displays severe weather climatology, which
we will discuss within the notebook.

All other netCDF files are monthly averaged re-analysis data from ERA5. These are in the form of temperature, vorticity, 
relative humidity, and u and v wind vectors, all from 1980-2019. The reason these years were chosen was to 
give a climatological sense of the data and be able to extract different findings from them, as well as to stay
under the 1GB limit for data.